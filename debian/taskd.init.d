#!/bin/sh

# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if "${INIT_D_SCRIPT_SOURCED:-false}"; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi

### BEGIN INIT INFO
# Provides:          taskd
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: taskwarrior sync server
# Description:       Multi-user sync server for taskwarrior clients
### END INIT INFO

DAEMON=/usr/bin/taskd
START_ARGS=--chuid Debian-taskd
DAEMON_ARGS=server --data /var/lib/taskd --daemon
